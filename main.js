
let numberOfFaces = 5; 

const theBody = document.getElementsByTagName("body")[0];

// The 2 sides of the game 
const theLeftSide = document.getElementById("leftSide");
const theRightSide = document.getElementById("rightSide");

// Function for generating smiley faces
const generateFaces = () => {
    // Loop for creating multiple faces based on the numberOfFaces
    for (i = 0; i < numberOfFaces; i++) { 
        // Creating a smiley face
        const img = document.createElement('img');
        img.src="./images/smile.png";
        

        // Assigning a random position to the smiley face
        img.style.top = Math.floor((Math.random() * 400)) + 'px';
        img.style.left = Math.floor((Math.random() * 400)) + 'px';
       
        // Appending the smileyface to the left div
        document.getElementById('leftSide').appendChild(img);    

        // Cloning the images to the from the left to the right with one smiley face removed
        const leftSideImages = theLeftSide.cloneNode(true);
        leftSideImages.removeChild(leftSideImages.lastChild);
        theRightSide.append(leftSideImages); 
    }
}

// Generating faces
generateFaces(); 

// Event handler for when the player successfully clicks the extra face
theLeftSide.lastChild.onclick = function nextLevel(event){
    // Ensure that the event is only fired on the lastChild:
    event.stopPropagation();
    numberOfFaces += 5;
    // Loop for removing previous smiling faces 
    while (numberOfFaces > 5) {
        theLeftSide.removeChild(theLeftSide.lastChild);
        theRightSide.removeChild(theRightSide.lastChild);
        numberOfFaces--; 
    }
    generateFaces();

};

// Function for handling the situation when the player clicks on anything except the correct face
theBody.onclick = function gameOver() {
    // Disabling the functionality of the game due to game over
    alert("Game Over!");
    theBody.onclick = null;
    theLeftSide.lastChild.onclick = null;
};

